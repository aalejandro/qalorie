#!/bin/bash
IMAGE=qalorie/qalorie_web
RELEASE=latest
docker build --no-cache -t $IMAGE:$RELEASE .
ID="$(docker images | grep $IMAGE | head -n 1 | awk '{print $3}')"
docker tag $ID $IMAGE:$RELEASE
docker tag -f $IMAGE:$RELEASE us.gcr.io/qalorie-1224/$IMAGE:$RELEASE
gcloud docker push us.gcr.io/qalorie-1224/$IMAGE:$RELEASE
