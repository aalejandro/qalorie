Docker setup for qalorie web app

To run this you need:

1. Install Docker Toolbox 
2. Copy your ssh private key to docker_files/id_rsa
3. docker-composer up

To rebuild:

1. git pull
2. docker-composer build
