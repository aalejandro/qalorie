import mongoose from 'mongoose'
import colors from "colors"
import conf from "../config"

// connect to mongodb database
class DB{
  connect(cb){
    let ip = conf.get("mongo.ip")
    let port = conf.get("mongo.port")
    let database = conf.get("mongo.database")
    let connString = `mongodb://qalorie-prod:G4KvaFjudfxcdf8E@ds025658-a0.mlab.com:25658,ds025658-a1.mlab.com:25658/qalorie-prod?replicaSet=rs-ds025658`
    console.log(connString)
    mongoose.connect(connString)
    var db = mongoose.connection
    db.on('error', (error) => console.log("[Mongo]".blue+" connection error: ".red, error) )
    db.once('open', function () {
      console.log("[Mongo]".blue+" connection success ".green + connString.yellow)
      if (cb != undefined && typeof cb == "function"){
        cb()
      }
    })
  }
}

export default new DB()
