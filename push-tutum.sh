#!/bin/bash
IMAGE=qalorie/qalorie_web
RELEASE=latest
docker build -t $IMAGE:$RELEASE .
ID="$(docker images | grep $IMAGE | head -n 1 | awk '{print $3}')"
docker tag $ID $IMAGE:$RELEASE
docker tag -f $IMAGE:$RELEASE tutum.co/aalejandro/qalorie:$RELEASE
docker push tutum.co/aalejandro/qalorie:$RELEASE
