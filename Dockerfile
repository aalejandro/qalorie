FROM debian
MAINTAINER Abel Alejandro <abel@aalejandro.com>

# Let apt-get know that there is no tty
ENV DEBIAN_FRONTEND noninteractive

# Copy debian src file
COPY ./docker_files/deb-src.list /etc/apt/sources.list.d/

# Bitbucket private ssh key, add your own.
COPY ./docker_files/id_rsa /root/.ssh/
RUN chmod 700 /root/.ssh && chmod 600 /root/.ssh/id_rsa

# Install packages
RUN apt-get update && apt-get dist-upgrade -y && \
    apt-get install -y git gcc g++ libssl-dev make wget build-essential python imagemagick && \
    apt-get install -y dos2unix && \
    cd /tmp && \
    wget https://nodejs.org/download/release/v0.10.34/node-v0.10.34.tar.gz && \
    tar zxf node-v0.10.34.tar.gz && \
    cd node-v0.10.34 && \
    ./configure && \
    make -j4 && \
    make install && \
    rm -rf /tmp/node* && \
    touch /root/.ssh/known_hosts

RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts && \
    mkdir /app && \
    cd /app && \
    git clone git@bitbucket.org:nullind/qalorie-backend.git

# Overwrite mongoose version from git repo
COPY ./docker_files/package.json /app/qalorie-backend/app/

RUN cd /app/qalorie-backend/app && \
    npm install && \
    npm install -g grunt-cli && \
    grunt

# Copy custom config file for the app using docker's db name
COPY ./docker_files/config.yaml /app/qalorie-backend/app/config/
COPY ./docker_files/db.js /app/qalorie-backend/app/lib/db.js

# Copy our executable to run the app inside the container
COPY ./docker_files/run.sh /
RUN chmod 755 /run.sh && dos2unix /run.sh
RUN chmod -R 755 /app/qalorie-backend/app/modules/webserver/public/uploads

# Run app
ENTRYPOINT ["/bin/bash", "/run.sh"]
